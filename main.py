from operator import index
import random
#from queue import Empty
import pygame
from pygame.locals import *
import numpy as np


taille = 150
size = (3*taille, 3*taille)


class Case:
    def __init__(self, index) -> None:
        self.index = index
        self.sum = 0

class Player:
    def __init__(self, type) -> None:
        self.type = type


class Plateau:

    def __init__(self) -> None:
        self.board = np.array([[0,0,0],[0,0,0],[0,0,0]])

    
    def best_case(self, nb, turn, board , p = 0):
        if not 1 in board and not 2 in board:
            return (random.randint(0,8),0)
        buffboard = np.copy(board)
        buffboard2 = np.copy(board)
        sum_list = []
        sum_idx = []
        for i in range(p, 9):
            (col, row) = divmod(i, 3)
            buffboard = np.copy(buffboard2)
            if buffboard[row][col] != 0:
                #sum_list[i] = -1
                continue
            else:
                sum_list.append(0)
                sum_idx.append(i)
                if turn:
                    
                    buffboard[row][col] = nb

                    (win, jr) = self.victoryb(buffboard)
                    if win and jr==nb:
                        sum_list[sum_idx.index(i)] = 1
                        continue

                    (inx, mx) = self.best_case(nb, False, buffboard , 0)
                    sum_list[sum_idx.index(i)] = mx

                    
                else:
                    if nb == 1:
                        buffboard[row][col] = 2
                    else:
                        buffboard[row][col] = 1

                    (win, jr) = self.victoryb(buffboard)
                    if win and jr!=nb and jr!=-1 and jr!=0:
                        sum_list[sum_idx.index(i)]= -1
                        continue
                    (inx, mx) = self.best_case(nb, True, buffboard, 0)
                    sum_list[sum_idx.index(i)] = mx
        
        if( not sum_list):
            return (0,0)

                    
        if not turn:
            return (sum_idx[sum_list.index(min(sum_list))],min(sum_list))
        else:
            return (sum_idx[sum_list.index(max(sum_list))],max(sum_list))

        #return (sum_list.index(max(sum_list)),sum(sum_list))
        


    def victory(self):
        if self.board[0][0] == self.board[0][1] and self.board[0][1] == self.board[0][2]:
            return (True, self.board[0][0])
        elif self.board[1][0] == self.board[1][1] and self.board[1][1] == self.board[1][2]:
            return (True, self.board[1][0])
        elif self.board[2][0] == self.board[2][1] and self.board[2][1] == self.board[2][2]:
            return (True, self.board[2][0])
        elif self.board[0][0] == self.board[1][0] and self.board[1][0] == self.board[2][0]:
            return (True, self.board[0][0])
        elif self.board[0][1] == self.board[1][1] and self.board[1][1] == self.board[2][1]:
            return (True, self.board[0][1])
        elif self.board[0][2] == self.board[1][2] and self.board[1][2] == self.board[2][2]:
            return (True, self.board[0][2])
        elif self.board[0][0] == self.board[1][1] and self.board[1][1] == self.board[2][2]:
            return (True, self.board[0][0])
        elif self.board[0][2] == self.board[1][1] and self.board[1][1] == self.board[2][0]:
            return (True, self.board[0][2])
        else:
            return (False, -1)

    def victoryb(self, board):
        if board[0][0] == board[0][1] and board[0][1] == board[0][2]:
            return (True, board[0][0])
        elif board[1][0] == board[1][1] and board[1][1] == board[1][2]:
            return (True, board[1][0])
        elif board[2][0] == board[2][1] and board[2][1] == board[2][2]:
            return (True, board[2][0])
        elif board[0][0] == board[1][0] and board[1][0] == board[2][0]:
            return (True, board[0][0])
        elif board[0][1] == board[1][1] and board[1][1] == board[2][1]:
            return (True, board[0][1])
        elif board[0][2] == board[1][2] and board[1][2] == board[2][2]:
            return (True, board[0][2])
        elif board[0][0] == board[1][1] and board[1][1] == board[2][2]:
            return (True, board[0][0])
        elif board[0][2] == board[1][1] and board[1][1] == board[2][0]:
            return (True, board[0][2])
        else:
            return (False, -1)

    def draw(self, screen):
        pygame.font.init()
        font = pygame.font.SysFont(None, int(taille/2))
        X = font.render('X', True, (255,0,0))
        O = font.render('O', True, (0,0,255))
        offset = taille/8
        for i in range(3):
            
            for j in range(3):
                
                pygame.draw.rect(screen, (255,255,255), (taille*i,taille*j,taille,taille))
                if(self.board[i][j] == 0):
                    pass
                elif(self.board[i][j] == 1):
                    screen.blit(X, (taille*i + taille/2 - offset, taille*j + taille/2 - offset))
                elif(self.board[i][j] == 2):
                    screen.blit(O, (taille*i + taille/2 - offset, taille*j + taille/2 - offset))
                if i!=0:
                    pygame.draw.line(screen, (0,0,0), (taille*i, 0), (taille*i, 3*taille), 2)
                if j!=0:
                    pygame.draw.line(screen, (0,0,0), (0, taille*j), (3*taille, taille*j), 2)

    
    def find_case(self, x, y):
        case_x = -1
        case_y = -1
        for i in range(3):
            for j in range(3):
                if(x > taille*i and x < taille*(i+1) and y > taille*j and y < taille*(j+1)):
                    case_x = i
                    case_y = j
                    break
        return (case_x, case_y)




screen = pygame.display.set_mode(size)
pygame.display.set_caption("Morpion")

plateau = Plateau()

game_over = False

joueur = Player(0)
ia = Player(1)
ia2 = Player(1)

ordre = [ia, ia2]
ordre = [ia, joueur]
#ordre = [joueur, ia]

joueur_actif = 0


while not game_over:
    # --- Main event loop

    (game_over, o) = plateau.victory()
    if not game_over and not 0 in plateau.board:
        game_over = True
    if o == 0:
        game_over = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_over = True
    

    if ordre[joueur_actif].type == 0: #joueur non IA
        if pygame.mouse.get_pressed()[0]:
            (x,y) = plateau.find_case(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
            if plateau.board[x][y] == 0:
                if(joueur_actif == 0):
                    plateau.board[x][y] = 1
                    joueur_actif = 1
                else:
                    plateau.board[x][y] = 2
                    joueur_actif = 0
    else : #joueur IA
        pygame.time.delay(1000)
        thisnb = 0
        if(joueur_actif == 0):
            thisnb = 1
        else:
            thisnb = 2
        (inx, rn) = plateau.best_case(thisnb, True, plateau.board)
        (y, x) = divmod(inx, 3)
        if plateau.board[x][y] == 0:
            if(joueur_actif == 0):
                plateau.board[x][y] = 1
                joueur_actif = 1
            else:
                plateau.board[x][y] = 2
                joueur_actif = 0
    

    
    plateau.draw(screen)
    pygame.display.update()

pygame.time.delay(1000)


#clock.tick(60)

pygame.quit()